<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;

class QuestionController extends Controller
{
    public function index() {
        $questions = Question::get();
        return view('questions.index', ['questions' => $questions]);
    }

    public function newQuestion() {
        return view('questions.new');
    }

    public function createQuestion(Request $request) {
        $question = new Question();
        $question->question = $request->get('question');
        $question->save();
        return redirect('/');
    }
}
