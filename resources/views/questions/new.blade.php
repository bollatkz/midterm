<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>New Question</title>
</head>
<body>
<form action="/create" method="POST">
    {{csrf_field()}}
    <textarea name="question" id="question" cols="30" rows="10"></textarea>
    <input type="submit">
</form>
</body>
</html>